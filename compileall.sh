function prepare_system() {
echo -e "Installing Dependencies,please be patient"
apt-get update 
DEBIAN_FRONTEND=noninteractive apt-get update > /dev/null 2>&1
DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" -y -qq upgrade 
apt install -y software-properties-common 
echo -e "${GREEN}Adding bitcoin PPA repository"
apt-add-repository -y ppa:bitcoin/bitcoin 
echo -e "Installing required packages, it may take some time to finish.${NC}"
apt-get update 
sudo apt-get install qtdeclarative5-dev qml-module-qtquick-controls  
sudo apt-get install qt5-default 
sudo apt-get install qt5-default qttools5-dev-tools  
sudo apt install python3 python 

sudo apt-get install g++-mingw-w64-i686 mingw-w64-i686-dev g++-mingw-w64-x86-64 mingw-w64-x86-64-dev 
apt-get install -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" make software-properties-common \
build-essential libtool autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev libboost-program-options-dev \
libboost-system-dev libboost-test-dev libboost-thread-dev sudo automake git wget curl libdb4.8-dev bsdmainutils libdb4.8++-dev \
libminiupnpc-dev libgmp3-dev ufw pkg-config libevent-dev  libdb5.3++ unzip libzmq5 

if [ "$?" -gt "0" ];
  then
    echo -e "${RED}Not all required packages were installed properly. Try to install them manually by running the following commands:${NC}\n"
    echo "apt-get update"
    echo "apt -y install software-properties-common"
    echo "apt-add-repository -y ppa:bitcoin/bitcoin"
    echo "apt-get update"
    echo "apt install -y make build-essential libtool software-properties-common autoconf libssl-dev libboost-dev libboost-chrono-dev libboost-filesystem-dev \
libboost-program-options-dev libboost-system-dev libboost-test-dev libboost-thread-dev sudo automake git curl libdb4.8-dev \
bsdmainutils libdb4.8++-dev libminiupnpc-dev libgmp3-dev ufw pkg-config libevent-dev libdb5.3++ unzip libzmq5"
 exit 1
fi
clear
}

# function create_swap() {
#  echo -e "Checking if swap space is needed."
#  PHYMEM=$(free -g|awk '/^Mem:/{print $2}')
#  SWAP=$(free -g|awk '/^Swap:/{print $2}')
#  if [ "$PHYMEM" -lt "2" ] && [ -n "$SWAP" ]
#   then
#     echo -e "${GREEN}Server is running with less than 2G of RAM without SWAP, creating 2G swap file.${NC}"
#     SWAPFILE=$(mktemp)
#     dd if=/dev/zero of=$SWAPFILE bs=1024 count=2M
#     chmod 600 $SWAPFILE
#     mkswap $SWAPFILE
#     swapon -a $SWAPFILE
#  else
#   echo -e "${GREEN}Server running with at least 2G of RAM, no swap needed.${NC}"
#  fi
#  clear
# }


function makedepends() {
     #make depends first for all platforms we need
cd depends
wget https://raw.githubusercontent.com/Bitcoin-Monster/BTCMonster/master/depends/Makefile
cd patches/qt/
wget https://raw.githubusercontent.com/bulwark-crypto/Bulwark/master/depends/patches/qt/aarch64-qmake.conf
cd ..
cd ..
chmod -R 775 *

echo '----Linux depends starting 64 bit -----'
python3 /root/cryptodx/message.py '----Linux depends starting 64 bit -----'
# make HOST=x86_64-pc-linux-gnu
# echo '----Linux ARM depends starting 64 bit -----'
# python3 /root/cryptodx/message.py  '----Linux ARM depends starting 64 bit -----'
# make HOST=arm-linux-gnueabihf
# echo '----Linux aarch64 starting 64 bit -----'
# python3 /root/cryptodx/message.py  '----Linux aarch64 starting 64 bit -----'
# make HOST=aarch64-linux-gnu
# echo '----Windows depends starting 64 bit -----'
# python3 /root/cryptodx/message.py '----Windows depends starting 64 bit -----'
# make HOST=x86_64-w64-mingw32
echo '----Windows depends starting 32 bit -----'
python3 /root/cryptodx/message.py '----Windows depends starting 32 bit -----'
make HOST=i686-w64-mingw32
cd ..
}
function makelinux() {
        python3 /root/cryptodx/message.py 'Linux wallets stared compiling'
        chmod -R 775 *
        # x86_64-pc-linux-gnu
./autogen.sh;
./configure --enable-tests=no --with-gui=qt5 LDFLAGS="-static"
make
mkdir -p build/v2.0.0.0/x86_64-pc-linux-gnu
cp /root/cryptodx/src/cryptodxd build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodxd
cp /root/cryptodx/src/cryptodx-tx build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodx-tx
cp /root/cryptodx/src/cryptodx-cli build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodx-cli
cp /root/cryptodx/src/qt/cryptodx-qt build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodx-qt
strip build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodxd
strip build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodx-tx
strip build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodx-cli
strip build/v2.0.0.0/x86_64-pc-linux-gnu/cryptodx-qt
 python3 /root/cryptodx/message.py 'Linux wallets finished'
# cd build/v2.0.0.0/x86_64-pc-linux-gnu
cd /root/cryptodx

make clean

}
function makewindows64() {
        # x86_64-w64-mingw32
                python3 /root/cryptodx/message.py 'Windows 64 bit wallets started compiling'
./autogen.sh;
./configure --prefix=/root/cryptodx/depends/x86_64-w64-mingw32 --enable-tests=no --with-gui=qt5
# make HOST=x86_64-w64-mingw32 -j4 -k;
make
mkdir -p build/v2.0.0.0/x86_64-w64-mingw32
cp /root/cryptodx/src/cryptodxd.exe build/v2.0.0.0/x86_64-w64-mingw32/cryptodxd.exe
cp /root/cryptodx/src/cryptodx-tx.exe build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-tx.exe
cp /root/cryptodx/src/cryptodx-cli.exe build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-cli.exe
cp /root/cryptodx/src/qt/cryptodx-qt.exe build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-qt.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodxd.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-tx.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-cli.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-qt.exe
# ## created detached signatures
# cd build/v2.0.0.0/x86_64-w64-mingw32;


# ##/C=   Country         GB
# ##/ST=  State   London
# ##/L=   Location        London
# ##/O=   Organization    Global Security
# ##/OU=  Organizational Unit     IT Department
# ##/CN=  Common Name     example.com

# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodx-qt-selfsigned.key -out ./cryptodx-qt-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net";
# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodxd-selfsigned.key -out ./cryptodxd-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net";
# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodx-tx-selfsigned.key -out ./cryptodx-tx-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net"; 
# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodx-cli-selfsigned.key -out ./cryptodx-cli-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net"; 

# osslsigncode sign -certs cryptodx-qt-selfsigned.crt -key cryptodx-qt-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodx-qt.exe -out cryptodx-qt-signed.exe

# osslsigncode sign -certs cryptodxd-selfsigned.crt -key cryptodxd-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodxd.exe -out cryptodxd-signed.exe

# osslsigncode sign -certs cryptodx-tx-selfsigned.crt -key cryptodx-tx-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodx-tx.exe -out cryptodx-tx-signed.exe

# osslsigncode sign -certs cryptodx-cli-selfsigned.crt -key cryptodx-cli-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodx-cli.exe -out cryptodx-cli-signed.exe

# mv cryptodx-qt-signed.exe cryptodx-qt.exe;
# mv cryptodxd-signed.exe cryptodxd.exe;
# mv cryptodx-tx-signed.exe cryptodx-tx.exe;
# mv cryptodx-cli-signed.exe cryptodx-cli.exe;

cd /root/cryptodx;
make clean
     python3 /root/cryptodx/message.py 'Windows 64 bit wallets finished compiling'
}
function makewindows32() {
        # x86_64-w64-mingw32
./autogen.sh;
./configure --prefix=/root/cryptodx/depends/i686-w64-mingw32 --enable-tests=no --with-gui=qt5
# make HOST=x86_64-w64-mingw32 -j4 -k;
python3 /root/cryptodx/message.py 'Windows 32 bit wallets started compiling'
make
mkdir -p build/v2.0.0.0/i686-w64-mingw32
cp /root/cryptodx/src/cryptodxd.exe build/v2.0.0.0/xi686-w64-mingw32/cryptodxd.exe
cp /root/cryptodx/src/cryptodx-tx.exe build/v2.0.0.0/i686-w64-mingw32/cryptodx-tx.exe
cp /root/cryptodx/src/cryptodx-cli.exe build/v2.0.0.0/i686-w64-mingw32/cryptodx-cli.exe
cp /root/cryptodx/src/qt/cryptodx-qt.exe build/v2.0.0.0/i686-w64-mingw32/cryptodx-qt.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodxd.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-tx.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-cli.exe
# strip build/v2.0.0.0/x86_64-w64-mingw32/cryptodx-qt.exe
# ## created detached signatures
# cd build/v2.0.0.0/x86_64-w64-mingw32;


# ##/C=   Country         GB
# ##/ST=  State   London
# ##/L=   Location        London
# ##/O=   Organization    Global Security
# ##/OU=  Organizational Unit     IT Department
# ##/CN=  Common Name     example.com

# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodx-qt-selfsigned.key -out ./cryptodx-qt-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net";
# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodxd-selfsigned.key -out ./cryptodxd-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net";
# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodx-tx-selfsigned.key -out ./cryptodx-tx-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net"; 
# openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout ./cryptodx-cli-selfsigned.key -out ./cryptodx-cli-selfsigned.crt -subj "/C=AT/ST=AK/L=AK/O=Development/OU=Core Development/CN=crypto-dex.net"; 

# osslsigncode sign -certs cryptodx-qt-selfsigned.crt -key cryptodx-qt-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodx-qt.exe -out cryptodx-qt-signed.exe

# osslsigncode sign -certs cryptodxd-selfsigned.crt -key cryptodxd-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodxd.exe -out cryptodxd-signed.exe

# osslsigncode sign -certs cryptodx-tx-selfsigned.crt -key cryptodx-tx-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodx-tx.exe -out cryptodx-tx-signed.exe

# osslsigncode sign -certs cryptodx-cli-selfsigned.crt -key cryptodx-cli-selfsigned.key \
#         -n "cryptodx Ltd" -i http://www.crypto-dex.net/ \
#         -t http://timestamp.verisign.com/scripts/timstamp.dll \
#         -in cryptodx-cli.exe -out cryptodx-cli-signed.exe

# mv cryptodx-qt-signed.exe cryptodx-qt.exe;
# mv cryptodxd-signed.exe cryptodxd.exe;
# mv cryptodx-tx-signed.exe cryptodx-tx.exe;
# mv cryptodx-cli-signed.exe cryptodx-cli.exe;

cd /root/cryptodx
python3 /root/cryptodx/message.py 'Windows 64 bit wallets finished compiling'
make clean

}
function ziprelease() {
python3 /root/cryptodx/message.py 'All wallets finished compiling,zipping them now and uploading'
cd /root/cryptodx
zip -r wallets.zip build
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi 
tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; } 
WALLETSLINK=$(transfer wallets.zip)
python3 /root/cryptodx/message.py 'Wallet has been uploaded,here is the link'
python3 /root/cryptodx/message.py $WALLETSLINK
}


prepare_system
makedepends
makelinux
makewindows32
# makewindows64
ziprelease
